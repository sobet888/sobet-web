import type { Metadata } from "next";
import { Kanit } from "next/font/google";
import "./globals.css";
import { Nav } from "@/components/nav";
import { Footer } from "@/components/footer";
import { GoogleAnalytics } from '@next/third-parties/google'
import Head from "next/head";


const kanit = Kanit({ subsets: ["thai"], weight: ["300", "700"] });

export const metadata: Metadata = {
	title: "พนันออนไลน์เว็บตรง #1คาสิโนออนไลน์เจ้าใหญ่ในไทย",
	description: "เว็บพนันที่ปลอดภัยสุดในไทย ครบทุกเกมการพนันออนไลน์ หวย กีฬา สล็อต บาคาร่า จบในเว็บเดียว ฝากถอนออโต้ไม่ต้องรอ เกมครบทุกค่าย จ่ายหนัก จ่ายจริง แตกง่ายกว่าใคร ต้องคาสิโนออนไลน์ที่Sobet888",
	openGraph: {
		type: "website",
		locale: "th_TH",
		url: "https://passtheplay.com",
		title: "พนันออนไลน์เว็บตรง #1คาสิโนออนไลน์เจ้าใหญ่ในไทย",
		description: "เว็บพนันที่ปลอดภัยสุดในไทย ครบทุกเกมการพนันออนไลน์ หวย กีฬา สล็อต บาคาร่า จบในเว็บเดียว ฝากถอนออโต้ไม่ต้องรอ เกมครบทุกค่าย จ่ายหนัก จ่ายจริง แตกง่ายกว่าใคร ต้องคาสิโนออนไลน์ที่Sobet888",
		images: [
			{
				url: "https://passtheplay.com/logo.jpg",
				width: 800,
				height: 600,
				alt: "Sobet888",
			},
		],
	},
	alternates: {
		canonical: "https://passtheplay.com",
		languages: {
			'th': 'https://passtheplay.com',
		}
	}
};

export default function RootLayout({
	children,
}: Readonly<{
	children: React.ReactNode;
}>) {
	return (
		<html lang="th">
			<Head>
				<title>พนันออนไลน์เว็บตรง</title>
			</Head>
			<body className={kanit.className}>
				<Nav />
				{children}
				<Footer />
			</body>
			<GoogleAnalytics gaId="G-NXTPP3BCLZ"/>
		</html>
	);
}
