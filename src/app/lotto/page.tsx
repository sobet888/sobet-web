import { RegisterBtn } from "@/components/registerBtn";
import Image from "next/image";
import { Metadata } from "next";
import lottoThai from "/public/lotto-thai.png";
import lottoYg from "/public/lotto-yg.png";
import banner from "/public/lotto-gif.gif";

export const metadata: Metadata = {
    title: "#1 เว็บหวยออนไลน์ ลอตเตอรี่ หวยรัฐบาล หวยหุ้น ไม่มีอั้น",
    description: "เว็บหวยออนไลน์ที่จ่ายหนักสุดในไทย ครบทุกประเภทหวย จ่ายหนัก จ่ายจริง ไม่มีเลขอั้น ต้องแทงหวยออนไลน์ที่Sobet888",
    openGraph: {
        type: "website",
        locale: "th_TH",
        url: "https://passtheplay.com/lotto",
        title: "#1 เว็บหวยออนไลน์ ลอตเตอรี่ หวยรัฐบาล หวยหุ้น ไม่มีอั้น",
        description: "เว็บหวยออนไลน์ที่จ่ายหนักสุดในไทย ครบทุกประเภทหวย จ่ายหนัก จ่ายจริง ไม่มีเลขอั้น ต้องแทงหวยออนไลน์ที่Sobet888",
        images: [
            {
                url: "https://passtheplay.com/logo.jpg",
                width: 800,
                height: 600,
                alt: "Sobet888",
            },
        ],
    },
    alternates: {
        canonical: "https://passtheplay.com/lotto",
        languages: {
            'th': 'https://passtheplay.com/lotto',
        }
    }
};

export default function Page() {
    const jsonLd = {
        "@context": "https://schema.org",
        "@type": "BlogPosting",
        headline: "#1 เว็บหวยออนไลน์ ลอตเตอรี่ หวยรัฐบาล หวยหุ้น ไม่มีอั้น",
        image: "https://passtheplay.com/logo.jpg",
        datePublished: "2024-14-05",
        author: {
            "@type": "Person",
            name: "Sobet888",
            url: "https://passtheplay.com/lotto",
        },
    };
    return <div>
        <div className="container mx-auto flex flex-col items-center py-16 px-6">
            <section>
                <script type="application/ld+json" dangerouslySetInnerHTML={{ __html: JSON.stringify(jsonLd) }} />
            </section>
            <div className="flex flex-col gap-6 md:w-2/3">
                <div className="flex flex-col gap-4 items-center">
                    <h1 className="text-4xl mb-6 text-[#FFD700] text-center">แทงหวยออนไลน์ 24 Lotto ทุกประเทศ</h1>
                    <div className="grid gap-6">
                        <Image src={banner} alt="lotto" width={1000} height={500} />
                        <div>
                            <p>เว็บหวยออนไลน์#1 2024 มีครบทุกหวยให้เสี่ยงโชค หวยยี่กี หวยไทย หวยลาว หวยฮานอย หวยมาเลย์ หวยธกส หวยออมสิน หวยหุ้น หวยญี่ปุ่น หวยฮ่องกง หวยจีน หวยไต้หวัน หวยเกาหลี หวยอินเดีย หวยอังกฤษ หวยลอตโต้อเมริกา หวยอียิปต์ จ่ายหนัก จ่ายจริง จ่ายเต็ม แทงได้ไม่อั้น
                            </p>
                            <div className="my-6">
                                <RegisterBtn />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="my-6 w-full h-[2px] bg-white"></div>
                <div className="flex flex-col gap-4 items-center">
                    <h2 className="text-3xl mb-6 text-[#FFD700] text-center">แทงหวยออนไลน์ดีอย่างไร?</h2>
                    <p>หากคุณต้องการมองหาร เว็บแทงหวยออนไลน์ ที่ปลอดภัยและมีให้เลือกแทงหลากหลาย สมัครเว็บ sobet888 ได้เลย
                        หวยออนไลน์ ดีอย่างไร คือ สะดวกสบาย สามารถกดฝาก-ถอนเงิน ได้รวดเร็ว แทงถูกจ่ายเยอะกว่าทั่วไป แทง 1 บาท ได้ถึง 950 บาทเลย
                        แทงหวยออนไลน์สามารถสร้างรายได้ให้แก่ผู้เล่นได้อย่างมหาศาล และสามารถเล่นได้ทุกที่ทุกเวลา ไม่ต้องเสียเวลาในการเดินทางไปซื้อหวย
                    </p>
                    <RegisterBtn />
                </div>
                <div className="my-6 w-full h-[2px] bg-white"></div>
                <div className="flex flex-col gap-4 items-center">
                    <h2 className="text-3xl mb-6 text-[#FFD700] text-center">แทงหวยออนไลน์ ได้เงินเยอะจริง ไม่มีเลขอั้น</h2>
                    <p>
                        เว็บ แทงหวยออนไลน์ ของเรานั้น แน่นอนว่าเป็นเว็บไซต์ที่ขึ้นชื่อเรื่องการแทงหวยเป็นอย่างมาก ไม่ว่าคุณอยากจะเล่น หวยไทย ฮานอย ลาว หรือหวยอื่นๆ ก็สามารถเลือกได้ตามต้องการ ทางเว็บการันตีความมั่นคงด้านการเงิน 100% เว็บหวยจ่ายเงินจริง สมัครง่ายรวมถึงยังมีช่องทางการเล่นหวยที่สะดวกเปิดให้บริการ 24 ชม.
                    </p>
                    <RegisterBtn />
                </div>
                <div className="my-6 w-full h-[2px] bg-white"></div>
                <div className="flex flex-col gap-4 items-center">
                    <h2 className="text-3xl mb-6 text-[#FFD700] text-center">บริการเว็บหวยออนไลน์กับเราดียังไง?</h2>
                    <p>สิ่งที่ผู้เล่นกังวลใจมากที่สุดในการซื้อหวยออนไลน์ก็คือ การที่ตัวเองจะไม่ได้รับการจ่ายเงิน เมื่อแทงหวยถูก ถ้าหากว่าคุณเลือกแทงหวยบนเว็บอื่น ก็อาจจะการันตีไม่ได้ว่าคุณจะได้รับเงินจริงหรือไม่ แต่ถ้าหากเลือกแทงผ่านเว็บไซต์ Sobet888.Com รับรองได้เลยว่ามีการจ่ายเงินจริง 100% ไม่จกตา เพราะเว็บนี้มีความมั่นคงด้านการเงินจ่ายเงินตรงเวลา แจ้งถอนเมื่อไหร่ ก็รอรับเงินได้เลยทันที
                    </p>
                    <RegisterBtn />
                </div>
                <div className="my-6 w-full h-[2px] bg-white"></div>
                <div className="flex flex-col gap-4 items-center">
                    <h2 className="text-3xl mb-6 text-[#FFD700] text-center">เว็บแทงหวยออนไลน์ ที่ได้รับความน่าเชื่อถือจากสมาชิกทุกคน</h2>
                    <p>การเลือกเว็บไซต์ เพื่อเข้าเล่นหวยจะต้องเลือกเล่นกับเว็บที่เชื่อถือได้เท่านั้น
                        ซึ่งเว็บหวยออนไลน์อันดับ 1 แห่งนี้ก็การันตีความน่าเชื่อถือ ที่ได้รับจากสมาชิก 100% คุณจะไม่ต้องกังวลใจเมื่อเล่นหวยออนไลน์อีกต่อไป ไม่ว่าคุณอยากจะซื้อหวยประเภทไหน และในช่วงเวลาใด
                        เว็บหวยออนไลน์อันดับ 1 ของเราก็ยินดีให้ริการตลอดทั้งวัน
                        เพราะเว็บเราเป็น เว็บหวยออนไลน์ 24 ชม. ที่เปิดให้เข้าเล่นหวยได้อย่างสะดวกสบายผ่านแพลตฟอร์มที่ทันสมัย มาพร้อมกับบริการฝาก – ถอนออโต้ที่ใช้งานง่าย
                    </p>
                    <RegisterBtn />
                </div>
                <div className="my-6 w-full h-[2px] bg-white"></div>
                <div className="flex flex-col gap-4 items-center">
                    <h2 className="text-3xl mb-6 text-[#FFD700] text-center">ข้อดีของการ ซื้อหวยผ่านเว็บออนไลน์ เว็บแทงหวยอันดับ1</h2>
                    <p>การซื้อหวย ทุกคนก็คาดหวังว่าจะได้รับเงินรางวัลกันอยู่แล้ว ดังนั้นสิ่งสำคัญที้เป็นหัวใจหลักของ หวยออนไลน์ ก็คือ การเลือกเว็บก่อนเล่น และแน่นอนว่าเว็บหวยออนไลน์อันดับ 1 ของเราก็พร้อมให้บริการแก่คุณ 24 ชั่วโมงผานทางเข้าหลักที่น่าเชื่อถือ</p>
                    <ul className="list-disc list-inside">
                        <li>ความปลอดภัย มั่นคง จ่ายจริง</li>
                        <li>เว็บหวยจ่ายจริง ให้บริการตลอด24 ชั่วโมง หมายถึง
                            เพียงสมัครสมาชิก ที่ Sobet888 ที่ให้บริการมั่นคง ไม่ผ่านตัวแทนใดๆ จึงมีความน่าเชื่อถือสูง ปลอดภัย มั่นคง เล่นได้ จ่ายจริง ไม่โกง</li>
                        <li>อัตราการจ่ายเงินรางวัลสูง</li>
                        <li>เกมหวยที่หลากหลาย</li>
                        <li>เว็บแทงหวยออนไลน์Sobet888 มีหวยไห้เลือกเดิมพันมากมายเช่น หวยหุ้น หวยลาว หวยฮานอย หวยยี่กี่ และอื่นๆ</li>
                        <li>สมัครสมาชิกง่าย
                            สมัครสมาชิก  รวดเร็ว ทันใจ ผ่านระบบอัตโนมัติ เพียงไม่กี่ขั้นตอน</li>
                        <li>แทงหวยออนไลน์ เว็บใหญ่ เว็บตรง บริการดีเยี่ยม มีแอดมินคอยดูแลตลอด 24 ชม.</li>
                        <li>บริการ หวยออนไลน์ ที่สุดและปลอดภัยที่สุด หวยออนไลน์เว็บตรง มีทีมงานคอยดูแลอย่างสม่ำเสมอ ด้วยแอดมิน มืออาชีพคอยให้บริการตลอด 24 ชั่วโมง หากติดขัดส่วนไหนติดต่อได้ตลอด24 ชั่วโมง เว็บตรงไม่ผ่านเอเย่นต์ 100% ไม่มีค่าใช้จ่ายและติดต่อง่าย ทางเรามีทีมงานคอยช่วยเหลือและคอยดูแลท่านอย่างแน่นอน เว็บหวยออนไลน์ ที่บริการครบจบในที่เดียว เว็บตรง ฝากถอน ไม่มี ขั้นต่ำ ทั้งเรื่องระบบการเงินและการบริการ ถ้าอยากได้รับรางวัลและความสะดวกในการใช้งาน แทงหวยออนไลน์กับเว็บตรงไม่ผ่านเอเย่นต์ 2024 พร้อมการติดต่อทีมงานง่ายๆ หวยออนไลน์เว็บตรง เว็บใหญ่ที่ดีที่สุด สมัครเป็นสมาชิกที่นี่เท่านั้น</li>
                    </ul>
                    <div className="grid md:grid-cols-2 gap-8">
                        <Image src={lottoThai} alt="lotto-thai" width={500} height={500} />
                        <Image src={lottoYg} alt="lotto-yg" width={500} height={500} />
                    </div>
                    <RegisterBtn />
                </div>
                <div className="my-6 w-full h-[2px] bg-white"></div>
            </div>
        </div>
    </div>
}