import Image from "next/image";
import slot from "/public/slot.gif";
import bac from "/public/baccarat-gif.gif";
import server from "/public/server.jpg";
import line from "/public/line.png";
import game from "/public/game.png";
import Link from "next/link";
import bank from "/public/bank.jpg"
import lotto from "/public/lotto-gif.gif"
import sport from "/public/ball.gif"
import { RegisterBtn } from "@/components/registerBtn";
import euroBanner from "/public/euro/banner.jpg";
import hiloHome from "/public/hilo.gif";

export default function Home() {
	const jsonLd = {
		"@context": "https://schema.org",
		"@type": "BlogPosting",
		headline: "พนันออนไลน์เว็บตรง #1คาสิโนออนไลน์เจ้าใหญ่ในไทย",
		image: "https://passtheplay.com/logo.jpg",
		datePublished: "2024-14-05",
		author: {
			"@type": "Person",
			name: "Sobet888",
			url: "https://passtheplay.com",
		},
	};
	return (
		<div className="w-full">
			<section>
				<script type="application/ld+json" dangerouslySetInnerHTML={{ __html: JSON.stringify(jsonLd) }} />
			</section>
			<div className="container mx-auto flex flex-col items-center py-16 px-6">
				<h1 className="text-5xl mb-6 text-[#FFD700]">พนันออนไลน์ คาสิโนออนไลน์ Sobet888</h1>
				<p>
					รวมเกมคาสิโนระดับแนวหน้าของโลกมาไว้ในเว็บเดียว สมัครฟรี เล่นง่าย ไม่ต้องโยกเงินไปมา ใช้เพียงกระเป๋าเดียวเข้าเล่นได้ทุกค่ายเกม รองรับการเล่นทุกแพลตฟอร์ม เว็บตรงไม่ผ่านเอเย่นต์ ถ่ายทอดสดจากต่างประเทศโดยตรง เพียงฝากเงินขั้นต่ำ 100 บาทก็สามารถเข้าเล่นได้ทันที ถอนไวไม่มีขั้นต่ำ
					<br />
					<br />
					ทางเว็บมีเกมให้เลือกลงทุนมากมาย ไม่ว่าจะเป็น <Link href="https://sabaccarat888.com">บาคาร่า</Link> Sa gaming, Sexy baccarat, สล็อต Pg Slot, ไฮโล, รูเล็ต, ป๊อกเด้ง, โป๊กเกอร์, น้ำเต้าปูปลา, เสือมังกร และอื่นๆ อีกมากมาย ไม่ว่าคุณอยากเล่นเกมไหน เรามีให้ครบ รวมไปถึงพนันกีฬาทุกประเภท แทงหวยไม่มีอั้น ครบทุกเลขดังเราจัดให้
					<br />
					<br />
					บริการฝาก-ถอนอัตโนมัติรวดเร็วฉับไวไม่เกิน 1 นาที ไม่ล็อคยูส เล่นได้ถอนง่ายทุกยอด 1 บาทก็ถอนได้ ทางเว็บเราเปิดให้บริการตลอด 24 ชั่วโมง ไม่มีวันหยุด สมัครสมาชิกครั้งแรก รับเครดิตฟรีทันที 100% พร้อมกับโปรโมชั่นมากมาย เช่น ชวนเพื่อนรับเครดิตเพิ่ม คืนยอดเสียสูงสุด 5% เล่นเว็บเราไม่มีเสีย มีแต่ได้กับได้ การันตีกำไรชัวร์
					<br />
					<br />
					ทางเราได้รวมค่ายเกมดังที่มีคาสิโนสด กีฬาออนไลน? คาสิโนออนไลน์ และ ค่ายเกมดัง เช่น SBOBET, SA GAMING, AE SEXY, PRAGMATIC PLAY, ASIA GAMING, JOKER GAMING, DRAGON SOFT, YGGDRASIL, PG และ YL FISHING ค่ายเกมยิงปลาที่ได้รับความนิยมในประเทศไทย ซึ่งแต่ค่ายนั้นท่านสามารถเดิมพันได้อย่างสะดวกรวดเร็ว
				</p>
				<div className="my-8">
					<RegisterBtn />
				</div>
				<div className="my-6 w-full h-[2px] bg-white"></div>
				<Link href="/euro2024">
					<h2 className="text-4xl my-6 text-[#FFD700]">แทงบอลยูโร ที่ไหนดี ต้องที่นี่ Sobet888</h2>
				</Link>
				<Link href="/euro2024"><Image src={euroBanner} alt="EURO 2024" width={1500} height={1000} /></Link>
				<div className="my-8">
					<RegisterBtn />
				</div>
				<div className="my-6 w-full h-[2px] bg-white"></div>
				<div className="grid md:grid-cols-2 gap-8 mt-8">
					<div>
						<Link href="/slot">
							<Image className="rounded-lg shadow-lg mx-auto" src={slot} alt="สล็อต" width={400} height={500} />
						</Link>
						<Link href="/slot" className="">
							<h2 className="text-2xl mt-4 underline mb-4 text-[#FFD700] text-center">สล็อต</h2>
						</Link>
						<p>
							เว็บสล็อตทำเงินได้จริงต้อง <Link href="https://ppslot777.com">สล็อตเว็บตรง</Link> ที่นี่ได้รับมาตรฐานระดับสากล เว็บสล็อตใหม่ล่าสุด เล่นเท่าไหร่ก็ได้รับรางวัลที่ดีที่สุด เนื่องจากได้รับการสนับสนุนจากเว็บใหญ่มาแรง 2024 ทั้ง Pragmatic Play, Joker Gaming, Dragoon Soft, Yggdrasil, PG Gaming, You Lian Gaming
						</p>
						<p className="mt-4">
							เล่นทุกเกมไม่ติดขัดและรายได้เพิ่มขึ้นรัวๆ ปั่นสล็อตได้อย่างอิสระและทำเงินได้จริง ถ้าสนใจการเล่นที่ไม่เป็นสองรองใคร พร้อมฟีเจอร์การเล่นที่ทันสมัยและได้เงินสม่ำเสมอ สล็อตออนไลน์
							การเข้ามาสมัครและใช้งานที่นี่คือคำตอบที่คุ้มสุดๆ SPIN แตกหนักและจัดเต็มทุกยอด สล็อตเว็บตรงฝากถอนไว อัตราการได้รับรางวัลสูงกว่าเว็บสล็อตทั่วไป ซึ่งผู้เล่นสามารถพิสูจน์ได้ด้วยตัวคุณเอง
						</p>
					</div>
					<div>
						<Link href="/baccarat">
							<Image className="rounded-lg shadow-lg mx-auto" src={bac} alt="บาคาร่า" width={480} height={500} />
						</Link>
						<Link href="/baccarat">
							<h2 className="text-2xl mt-4 underline mb-4 text-[#FFD700] text-center">บาคาร่า</h2>
						</Link>
						<p>
							หนึ่งในเกมคาสิโนที่ได้รับความนิยมอย่างมาก คือ บาคาร่า ซึ่งมีกติกาการเล่นที่ง่ายและคล้ายคลึงกับไพ่ป๊อกเด้ง โดยผู้เล่นสามารถวางเดิมพันได้ในสามแบบคือ ชนะ, แพ้, และเสมอ ฝั่งที่มีแต้มรวมใกล้เคียงหรือเท่ากับ 9 แต้มจะเป็นฝ่ายชนะและได้รับเงินเดิมพันไป เราคือเว็บไซต์ที่รวมเกมคาสิโนทุกแพลตฟอร์มไว้ที่นี่ที่เดียว นอกจากบาคาร่าแล้ว เรายังมีเกมอื่นๆ เช่น เสือมังกร, สล็อต, รูเล็ต และอีกมากมายให้คุณได้เลือกเล่น
						</p>
						<p className="mt-4">คู่มือเกมไพ่บาคาร่าเป็นวิธีการเล่นที่ค่อนข้างง่าย ผู้เล่นเพียงแค่เดิมพันระหว่างฝั่งผู้เล่น (Player) หรือฝั่งเจ้ามือ (Banker) เท่านั้น โดยเกมนี้มีรูปแบบการเล่นที่คล้ายกับไพ่ป๊อกเด้ง ผู้เล่นจะต้องทายว่าฝั่งใดจะได้แต้มใกล้เคียงกับ 9 มากที่สุดเพื่อเป็นผู้ชนะ นอกจากนี้ยังมีความเป็นไปได้ที่จะเกิดผลเสมอในบางครั้ง เมื่อเกมบาคาร่าเริ่มขึ้น เจ้ามือจะทำการแจกไพ่ให้ทั้งสองฝั่ง คือฝั่งผู้เล่นและฝั่งเจ้ามือ</p>
					</div>
					<div>
						<Link href="/lotto">
							<Image className="rounded-lg shadow-lg mx-auto" src={lotto} alt="หวยออนไลน์" width={500} height={500} />
						</Link>
						<Link href="/lotto">
							<h2 className="text-2xl mt-4 underline mb-4 text-[#FFD700] text-center">หวยออนไลน์</h2>
						</Link>
						<p>
							ซื้อ<Link href="https://lotteryonline888.com">หวยออนไลน์</Link>กับ sobet888 สั่งซื้อกับเว็บตรง ไม่มีหัก ไม่มีค่าธรรมเนียม อัตราจ่ายสูง จ่ายเต็ม ไม่มีเลขอั้น ถ้าคุณกำลังมองหาเว็บซื้อหวยออนไลน์ที่ดีที่สุด ต้องที่ sobet888 ที่นี่คุณสามารถซื้อหวยได้ทุกแบบ เล่นได้ทุกเลข ไม่ว่าจะเป็นหวยใต้ดิน 2 ตัวจ่ายสูง และ 3 ตัวบาทละ 1000 โอนไว แทงหวยบนระบบที่ถูกกฎหมายในต่างประเทศ
						</p>
						<p className="mt-4">
							เว็บ sobet888 เป็นเว็บซื้อหวยออนไลน์ครบวงจร ที่สามารถแทงหวยได้ทั้งรัฐบาลไทย, หวยใต้ดิน, เลขหุ้น, หวยลาว, หวยฮานอย, หวยยี่กี, และหวยปิงปอง ซึ่งลุ้นได้ทุก 15 นาที สนุกได้ตลอด 24 ชั่วโมง แฟนหวยชาวไทยที่อยากสนุกกับเว็บบริการอันดับ 1 สามารถแทงหวยออนไลน์ได้ทุกวัน มีโปรโมชั่นเยอะและดีๆ ที่น่าสนใจตลอด
						</p>
						<p className="mt-4">
							เชิญทุกท่านสมัครสมาชิกพร้อมสนุกกับเว็บขายหวยรายใหญ่ รับซื้อโดยตรง พร้อมทีมงานมืออาชีพที่จะดูแลคุณอย่างใกล้ชิด เปิดให้บริการสั่งซื้อผ่านช่องทางออนไลน์ อัพเดทการแทงหวยล่าสุด คลิกเข้าใช้งานได้ที่นี่
						</p>
					</div>
					<div>
						<Link href="/sport">
							<Image className="rounded-lg shadow-lg mx-auto" src={sport} alt="พนันกีฬา" width={400} height={500} />
						</Link>
						<Link href="/sport">
							<h2 className="text-2xl mt-4 underline mb-4 text-[#FFD700] text-center">กีฬา</h2>
						</Link>
						<p>
							Sobet888 ยินดีต้อนรับทุกท่านเข้าสู่<Link href="https://tangball777.com">แทงบอลออนไลน์</Link>กับเจ้าใหญ่ในไทย เว็บแทงบอลที่ดีที่สุด อันดับ 1 ที่มีคนเล่นเยอะมากกว่าใคร ระบบสมบูรณ์แบบ เปิดลีกฟุตบอลออนไลน์มากกว่าใคร ทั้งลีกเล็ก ลีกใหญ่ บอลไลฟ์ บอลสเต็ป ทั่วโลก เริ่มต้นแทงบอลขั้นต่ำเพียง 10 บาทเท่านั้น เราให้ราคาบอลดีกว่าใคร เปิดราคาครบทุกตลาด ไม่ว่าจะเป็น Asian Handicap (ต่อลูก), สูง-ต่ำ, คู่-คี่, ประตูรวม, เตะมุม, 1×2, แทงบอลสเต็ป และแทงบอลสเต็ปคอมโบ 2-12 คู่
						</p>
						<p className="mt-4">
							แทงบอลไม่ผ่านเอเย่นต์ ให้บริการแทงบอลครบวงจรที่สุด เลือกเล่นรายการแข่งขันได้หลากหลายประเภทไม่อั้น มีให้เลือกเล่นครบทุกประเภท เว็บแทงบอลใหม่ล่าสุด 2024 ของเรามีการนำระบบเว็บพนันที่ทันสมัย สนุกครบทุกเกมในเว็บเดียว การันตีโบนัสรางวัลก้อนโต ระบบ auto วอเลทใหม่ล่าสุด ฝากถอนออโต้ รองรับทั้งระบบปฏิบัติการ IOS และ Android รวมถึงโน้ตบุ๊คและ iPad สร้างรายได้ทุกวันไม่มีวันหยุด พร้อมให้บริการตลอด 24 ชั่วโมง เดิมพันกีฬาออนไลน์ ทำเงินไว เชื่อถือได้ เล่นกับเว็บของเราเท่านั้น
						</p>
					</div>
					<div>
						<Link href="/hilo">
							<Image className="rounded-lg shadow-lg mx-auto" src={hiloHome} alt="พนันกีฬา" width={500} height={500} />
						</Link>
						<Link href="/hilo">
							<h2 className="text-2xl mt-4 underline mb-4 text-[#FFD700] text-center">ไฮโล</h2>
						</Link>
						<p>
							Sobet888 เว็บทอยลูกเต๋า ไฮโลไทยออนไลน์ เว็บตรงSicbo โอกาสแตกง่าย ได้เยอะจริง ฝาก-ถอนฟรี ไม่มีขั้นต่ำ ฝาก-ถอนออโต้24ชม. ไฮโล (Hi-Lo) เป็นเกมการพนันชนิดหนึ่งที่มีต้นกำเนิดมาจากประเทศจีน โดยมีกฎและรูปแบบการเล่นที่ง่าย ผู้เล่นสามารถเดิมพันผลลัพธ์ของลูกเต๋าสามลูกที่จะถูกทอยออกมา Sobet888 เป็นเว็บไซต์ที่ได้รับใบอนุญาตจากSicbo ทำให้ผู้เล่นมั่นใจได้ว่าเล่นกับเว็บไซต์ที่มีความโปร่งใสและยุติธรรม
						</p>
						<p className="mt-4">
							เว็บไฮโลครบวงจรของเรามีความนิยมในกลุ่มนักเล่นพนันจำนวนมาก ท่านสามารถเล่นผ่านโทรศัพท์มือถือและทำเงินได้ทุกวัน เพราะเราเป็นที่รู้จักในด้านการบริการที่ดีเยี่ยมและการสร้างรายได้ที่มั่นคง เว็บของเรายอดนิยมมากที่สุดในตอนนี้ ผู้ใช้สามารถเข้าใช้งานเว็บไซต์ได้อย่างง่ายดาย รวดเร็ว และสะดวกสบาย เพียงแค่ทายผลบอลถูกก็สามารถรับรางวัลมากมาย เราให้บริการสมาชิกมาอย่างยาวนาน มีประสบการณ์ และพัฒนาบริการให้ตอบสนองความต้องการของลูกค้าอย่างดีที่สุด
						</p>
					</div>
				</div>
				<div className="my-8">
					<RegisterBtn />
				</div>
				<div className="my-6 w-full h-[2px] bg-white"></div>
				<h2 className="text-[#FFD700] underline text-3xl mt-8">ทำไมต้องเล่นพนันออนไลน์กับ Sobet888</h2>
				<div className="grid md:grid-cols-3 gap-6 py-6">
					<div className="bg-black py-4 px-6 rounded-lg shadow-xl">
						<h3 className="text-2xl underline text-[#FFD700] mb-4">ไม่หนี ไม่บิด ถอนไม่อั้น</h3>
						<p>
							เว็บคาสิโนออนไลน์เจ้าใหญ่ ทุนหนา รับลูกค้าได้หมดไม่ว่าจะทุนน้อยหรือทุนหนา สมัครเล่นได้ทุกค่ายเกม ไม่ต้องเสียเวลาไปหาเว็บอื่น ที่นี่เรามีทุกค่ายเกมดัง ไม่ว่าจะเป็น บาคาร่า, สล็อต, หวย, กีฬา และอื่นๆ อีกมากมาย
						</p>
					</div>
					<div className="bg-black py-4 px-6 rounded-lg shadow-xl">
						<h3 className="text-2xl underline text-[#FFD700] mb-4">ฝากถอนออโต้</h3>
						<p>
							ฝากถอนทุกอย่างออโต้ เชื่อมต่อกับธนาคารที่มีชื่อเสียง ทำให้การฝากถอนเป็นไปอย่างรวดเร็ว ไม่ต้องรอนาน ไม่ต้องรอคิว ทำเงินได้จริง ไม่มีขั้นต่ำ ไม่ต้องแชทหาแอดมินให้เมื่อยมือ
						</p>
					</div>
					<div className="bg-black py-4 px-6 rounded-lg shadow-xl">
						<h3 className="text-2xl underline text-[#FFD700] mb-4">ลิขสิทธิ์แท้ เว็บตรงจากค่ายเกม</h3>
						<p>
							เราได้รับลิขสิทธิ์แท้จากหลากค่ายเกมดัง ไม่มีการโกง ไม่มีการแก้ไขรางวัล ทำให้คุณมั่นใจได้ว่าเราเป็นเว็บที่มีความน่าเชื่อถือสูงสุด
						</p>
					</div>
					<div className="bg-black py-4 px-6 rounded-lg shadow-xl">
						<h3 className="text-2xl underline text-[#FFD700] mb-4">มีทุกค่ายเกมดัง</h3>
						<p>
							อยากพนันอะไร เรามีเกมให้เล่นทุกแบบทุกสไตล์ ไม่ว่าจะเป็น บาคาร่า, สล็อต, หวย, กีฬา และอื่นๆ อีกมากมาย ที่นี่คุณสามารถเล่นเกมที่คุณชอบได้ทุกเกม
						</p>
					</div>
					<div className="bg-black py-4 px-6 rounded-lg shadow-xl">
						<h3 className="text-2xl underline text-[#FFD700] mb-4">ทุนน้อยเล่นได้ ไม่มีขั้นต่ำ</h3>
						<p>
							เล่นขั้นต่ำเพียง1บาท ไม่ต้องกังวลเรื่องทุน ทำเงินได้จริง ไม่ต้องกังวลเรื่องขั้นต่ำ เล่นขำๆก็ได้ เล่นเพื่อการลงทุนไม้ใหญ่ๆก็ดี จ่ายหนัก จ่ายจริง ไม่มีบิด ไม่มีหนี ถอนได้ทุกยอด ไม่มีขั้นต่ำ
						</p>
					</div>
					<div className="bg-black py-4 px-6 rounded-lg shadow-xl">
						<h3 className="text-2xl underline text-[#FFD700] mb-4">โอกาสแตกเยอะกว่าเว็บอื่น</h3>
						<p>
							เรารวบรวมเกมที่มีโอกาสแตกเยอะที่สุด ไม่ว่าจะเป็น บาคาร่า, สล็อต, หวย, กีฬา และอื่นๆ อีกมากมาย ที่นี่คุณสามารถเล่นเกมที่มีโอกาสแตกเยอะที่สุดได้ทุกเกม
						</p>
					</div>
				</div>
				<div className="my-8">
					<RegisterBtn />
				</div>
				<div className="my-6 w-full h-[2px] bg-white"></div>
				<h2 className="text-[#FFD700] underline text-3xl mt-8">วิธีเล่น</h2>
				<div className="py-6 w-full">
					<div className="py-4 px-6">
						<p>
							ขั้นตอนการเล่นคาสิโนออนไลน์บน Sobet888 เว็บสล็อตทำเงินได้จริงต้อง สล็อตเว็บตรง ที่นี่ได้รับมาตรฐานระดับสากล เว็บสล็อตใหม่ล่าสุด เล่นเท่าไหร่ก็ได้รับรางวัลที่ดีที่สุด เนื่องจากได้รับการสนับสนุนจากเว็บใหญ่มาแรง 2024
							ปั่นสล็อตได้อย่างอิสระและทำเงินได้จริง
						</p>
						<ol className="list-decimal list-inside">
							<li>เข้าหน้าหลัก<Link className="underline" href="https://sobet888.com">Sobet888</Link> หรือ <Link className="underline" href="https://sobet888.com/register_full/sb8">คลิกที่นี่เพิ่มไปสู่ขั้นตอนการสมัครสมาชิก</Link></li>
							<li>กรอกแบบฟอร์มตามขั้นตอน</li>
							<li>เมื่อเสร็จสมบูรณ์ สามารถฝากเงินผ่านบัญชีธนาคารใดก็ได้เพื่อทำการเริ่มเล่นได้เลย</li>
						</ol>
					</div>
					<div className="my-8">
						<RegisterBtn />
					</div>
				</div>
				<div className="my-6 w-full h-[2px] bg-white"></div>
				<div className="py-6 w-full">
					<h3 className="text-3xl underline text-[#FFD700] mb-4 text-center">ความปลอดภัยสูงสุด</h3>
					<div className="grid md:grid-cols-2 gap-6 mt-6">
						<div>
							<p>
								ที่ sobet888 เราให้ความสำคัญสูงสุดกับความปลอดภัยของข้อมูลและความเป็นส่วนตัวของคุณ ด้วยเทคโนโลยีการเข้ารหัสขั้นสูงและมาตรการป้องกันการฉ้อโกงที่เข้มงวด
								คุณสามารถวางใจได้ว่าข้อมูลส่วนตัวและการทำธุรกรรมทางการเงินของคุณจะได้รับการปกป้องอย่างดีที่สุด
							</p>
							<ul className="list-disc list-inside mt-6 space-y-4">
								<li>
									<span className="font-semibold">ระบบการเข้ารหัสข้อมูลขั้นสูง</span> – ปกป้องข้อมูลของคุณทุกครั้งที่คุณเข้าระบบ
								</li>
								<li>
									<span className="font-semibold">การชำระเงินที่ปลอดภัย</span> – รองรับวิธีการชำระเงินที่หลากหลาย พร้อมมาตรการป้องกันการฉ้อโกง
								</li>
								<li>
									<span className="font-semibold">การสนับสนุนผู้เล่นตลอด 24 ชั่วโมง</span> – บริการลูกค้าพร้อมช่วยเหลือคุณทุกเมื่อ
								</li>
								<li>
									<span className="font-semibold">นโยบายความเป็นส่วนตัวชัดเจน</span> – ปกป้องและใช้ข้อมูลของคุณอย่างโปร่งใส
								</li>
								<li>
									<span className="font-semibold">ข้อมูลไม่รั่วไหล</span> – เรามุ่งมั่นที่จะรักษาข้อมูลของคุณให้ปลอดภัยเสมอ
								</li>
								<li>
									<span className="font-semibold">ไม่มีนโยบายขายข้อมูล</span> - ข้อมูลส่วนตัวของคุณจะปลอดภัย เพราะเราไม่ขายข้อมูลต่อให้ใครทั้งนั้น
								</li>
								<li>
									<span className="font-semibold">กระบวนการทำลายข้อมูล</span> - คุณสามารถแน่ใจได้ว่าข้อมูลส่วนตัวทุกอย่างจะถูกลบเมื่อคุณต้องการ
								</li>
								<li>
									<span className="font-semibold">เซิร์ฟเวอร์AWS</span> - ปลอดภัยหายห่วงด้วยการป้องกันระดับโลกจากAmazon
								</li>
							</ul>
						</div>
						<div>
							<Image className="rounded-lg shadow-lg" src={server} alt="server" />
						</div>
					</div>
					<div className="my-8">
						<RegisterBtn />
					</div>
				</div>
				<div className="my-6 w-full h-[2px] bg-white"></div>
				<h2 className="text-[#FFD700] underline text-3xl my-8">รองรับการฝากเงินจากทุกธนาคารชั้นนำ</h2>
				<Image className="rounded-lg shadow-lg" src={bank} alt="bank" />
				<div className="my-8">
					<RegisterBtn />
				</div>
				<div className="my-6 w-full h-[2px] bg-white"></div>
				<h2 className="text-[#FFD700] underline text-3xl my-8">โปรโมชั่นพนันออนไลน์</h2>
				<p className="text-center">สมัครสมาชิกครั้งแรกรับเครดิตฟรีทันที100% ชวนเพื่อนเล่นด้วยกันรับเพิ่มอีก50% เครดิตจะได้รับในรูปแบบ uDiamond (เพรช) จะสะสมไว้ลุ้นรางวัลใหญ่ iPhone 15 Pro หรือสะสมไว้เป็นเครดิตถอนเข้าบัญชีได้เลย ชวนเพื่อนรับเครดิตเพิ่ม คืนยอดเสียสูงสุด5% เล่นเว็บเราไม่มีเสีย มีแต่ได้กับได้การันตีกำไรชัวร์</p>
				<div className="my-8">
					<RegisterBtn />
				</div>
				<div className="my-6 w-full h-[2px] bg-white"></div>
				<h2 className="text-[#FFD700] underline text-3xl my-8">ค่ายเกมมากมายให้คุณเลือก</h2>
				<p className="text-center">รวมเกมทุกค่ายดังๆระดับโลกมาไว้ในที่เดียว ให้คุณได้สนุกเพลิดเพลินไปกับประสบการณ์การพนันบนโลกออนไลน์อย่างไร้ขีดจำกัด</p>
				<div className="grid md:grid-cols-3 md:gap-6 py-4">
					<ul className="list-disc list-inside">
						<li>Sa Gaming</li>
						<li>PG Slot</li>
						<li>Joker Gaming</li>
						<li>Pragmatic Play</li>
						<li>Red Tiger</li>
						<li>AE Sexy</li>
						<li>Dragoon Soft</li>
						<li>EBET</li>
						<li>LOTTOHOT</li>
					</ul>
					<ul className="list-disc list-inside">
						<li>SBOBET</li>
						<li>UFABET</li>
						<li>BTi</li>
						<li>NAGA GAMES</li>
						<li>KA Gaming</li>
						<li>QTECH GAMES</li>
						<li>DREAM GAMING</li>
						<li>Evolution Gaming</li>
						<li>R88</li>
					</ul>
					<ul className="list-disc list-inside">
						<li>JDB</li>
						<li>KINGMAKER</li>
						<li>FA CHAI</li>
						<li>SpiniX</li>
						<li>bcoongo</li>
						<li>CQ9</li>
						<li>ONEGAME</li>
						<li>JILI</li>
					</ul>
				</div>
				<Image className="" src={game} alt="game" />
				<div className="my-8">
					<RegisterBtn />
				</div>
				<div className="my-6 w-full h-[2px] bg-white"></div>
				<h2 className="text-[#FFD700] underline text-3xl my-8">ติดต่อเรา</h2>
				<p className="mb-8 text-center">ติดต่อทีมงานSobet888 แอดไลน์มาได้เลย ไม่ว่าจะเปิดบัญชี สอบถามโปรโมชั่น แจ้งปัญหา ฝากถอน ปรึกษาปัญหาความรัก หรือ อื่นๆ 24ชม. Sobet888เว็บคาสิโนออนไลน์เว็บตรงไม่ผ่านเอเย่นต์ มีกิจกรรมแจกเครดิตฟรีไม่ต้องฝากเงิน มีเกมพนันทุกรูปแบบครบวงจรดีที่สุดในไทย ฝากถอนออโต้ 1วิ สมัครง่ายรับ USER ทดลองเล่นทันที โหลดแอปบนมือถือได้ง่ายทั้ง Ios และ android พนันเริ่มต้น 1 บาท</p>
				<Image className="rounded-lg shadow-lg" src={line} alt="line" />
				<div className="my-8">
					<RegisterBtn />
				</div>
			</div>
		</div>
	);
}
