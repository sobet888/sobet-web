import logo from '/public/logo.jpg';
import Image from 'next/image';
import Link from 'next/link';

export const Footer = () => {
    return (
        <footer className="bg-black shadow-lg overflow-hidden flex flex-col md:fixed md:bottom-0 gap-2 items-center py-8 md:py-2 w-full">
            <div className="flex flex-col md:flex-row items-center gap-4 md:gap-10">
                <Link href="/">
                    <Image src={logo} alt="logo" width={160} height={120} />
                </Link>
                <ul className='flex flex-col items-center md:flex-row gap-3 md:gap-6'>
                    <li>
                        <Link href="/slot" className="">สล็อต</Link>
                    </li>
                    <li>
                        <Link href="/baccarat" className="">บาคาร่า</Link>
                    </li>
                    <li>
                        <Link href="/lotto" className="">หวย</Link>
                    </li>
                    <li>
                        <Link href="/sport" className="">กีฬา</Link>
                    </li>
                    <li>
                        <Link href="/euro2024" className="">EURO 2024</Link>
                    </li>
                    <li>
                        <Link href="/hilo" className="">ไฮ-โล</Link>
                    </li>
                    <li>
                        <Link href="https://sobet888.com/register_full/sb8">
                            <button
                                className="bg-yellow-500 hover:bg-yellow-600 text-white text-xl py-2 px-4 rounded-lg transition duration-300 w-fit"
                            >
                                ลงทะเบียนเลย!
                            </button>
                        </Link>
                    </li>
                </ul>
            </div>
        </footer>
    );
}