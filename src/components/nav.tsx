import logo from '/public/logo.jpg';
import Image from 'next/image';
import Link from 'next/link';

export const Nav = () => {
    return (
        <nav className="bg-black shadow-lg overflow-hidden flex flex-col gap-2 items-center py-4">
            <div>
                <Link href="/">
                    <Image src={logo} alt="logo" width={160} height={120} />
                </Link>
            </div>
            <div>
                <ul className="flex flex-col md:flex-row md:space-x-4">
                    <li>
                        <Link href="/slot" className="">สล็อต</Link>
                    </li>
                    <li>
                        <Link href="/baccarat" className="">บาคาร่า</Link>
                    </li>
                    <li>
                        <Link href="/lotto" className="">หวย</Link>
                    </li>
                    <li>
                        <Link href="/sport" className="">กีฬา</Link>
                    </li>
                    <li>
                        <Link href="/euro2024" className="">EURO 2024</Link>
                    </li>
                    <li>
                        <Link href="/hilo" className="">ไฮ-โล</Link>
                    </li>
                </ul>
            </div>
        </nav>
    );
}
