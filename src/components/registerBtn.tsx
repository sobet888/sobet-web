import Link from "next/link";

export const RegisterBtn = () => {
    return (
        <Link href="https://sobet888.com/register_full/sb8">
            <button
                className="bg-yellow-500 hover:bg-yellow-600 text-white text-xl py-4 px-8 rounded-lg transition duration-300 w-fit"
            >
                สมัครเลย!
            </button>
        </Link>
    );
}